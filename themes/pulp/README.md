# pulp
![logo](https://user-images.githubusercontent.com/17229643/55247565-50245180-528b-11e9-9947-aa3c54ea05bb.png)  

Pulp is a [Hugo](https://gohugo.io/) theme for getting a simple, easy-to-read blog site.
