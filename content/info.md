# <a id="About"></a>About 👨‍💼 #

Hello and welcome, dear guests!

I am a __technology__ __enthusiast__ who was born in Siberia, Russia and now lives in North Holland, the Netherlands. 

This is my personal website where you can find my [contact details](#Links), some <a href="/other" target="_blank" rel="noopener noreferrer">information </a> about me and the services I use and recommend.

In the near future I will also list all the projects I have worked on.


# <a id="Links"></a>Links 🔗 #

<p> <a href="mailto:kirill@sundyrtsev.com" target="_blank" rel="noopener noreferrer">Mail</a> me</p> 

<p> <a href="https://keybase.io/kiriller" target="_blank" rel="noopener noreferrer">Message</a> me in Keybase</p>

<p> <a href="https://vsco.com/krllpht" target="_blank" rel="noopener noreferrer">Check</a> out my VSCO</p> 


## ⏭️
To go to another page, please <a href="/other" target="_blank" rel="noopener noreferrer">click here</a>.


<p style="text-align: center;"><sub><sup>This site was built with the help of Hugo theme Pulp and Gitlab.</sup></sub></p>
