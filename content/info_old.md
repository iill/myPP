# <a id="About"></a>About 👨‍💼 #

Hello and welcome, dear guests!

I am a __sysadmin__ who was born in Siberia and now lives in the Netherlands, <a href="https://www.holland.com/global/tourism/holland-stories/golden-age/amsterdam-capital-of-the-golden-age.htm" target="_blank">Amsterdam</a>.

This is my personal site, where you can find some information about me, a list of [my projects](#Projects) and my [contact details](#Links).

#### My interests
include system administration, pentesting, cloud computing, web development and all the new technologies in the IT world. 

#### My hobbies
 are cars, drones and urban planning.

#### My spare time
I like to devote myself to self-learning, traveling, and coming up with projects that are interesting and useful not only for me, but also for other people.

## Skills 🤹🏻
Admin:
Windows Server, Active Directory, MS SQL, MS Exchange, Postfix, Linux Server, PostreSQL/MariaDB, Docker, VMware ESXi, McAfee/Kaspersky, Networking Management, Backup Management, Bash Scripting, YAML/TOML.

Pentest:
nmap, Wireshark, Metasploit, Burp Suite.

Other: fundamentals of HTML, CSS and AWS.
 
<script src="https://tryhackme.com/badge/1054836"></script>

# <a id="Projects"></a>Projects 👨‍💻 #

*I have a lot of ideas in my head all the time, and I've decided to put some of the implemented projects on my personal page, step by step, to keep track of the progress. Here you will mainly find my dive into web development. I like websites based on minimalism, quality and fast response.*

### 1. Family website

I have a surname that is difficult to pronounce not only in English, but also in Russian. I am often asked why I have such a surname and where it comes from. I researched the history of the surname and with the help of the Institute of Genealogy & Heraldry some details were collected and the family coat of arms was created, so I decided to put everything on the family website.

- I made <a href="/old_site" target="_blank" rel="noopener noreferrer">the previous site</a> when I didn't know much about building sites. So I took the easy way out and chose Wordpress and various extensions like <a href="https://elementor.com" target="_blank" rel="noopener noreferrer">Elementor</a>. Before that, I only had the experience of making websites with <a href="https://www.ucoz.com/" target="_blank" rel="noopener noreferrer">Ucoz</a> for my school class.

- The new version I made in different language versions - __RU__ and __ENG__.

  - <a href="https://sundyrtsev.ru" target="_blank" rel="noopener noreferrer">RU</a> is still being perfected.

   - <a href="https://sundyrtsev.com" target="_blank" rel="noopener noreferrer">ENG</a> is minimalistic and was created with the help of <a href="https://astro.build/" target="_blank" rel="noopener noreferrer">Astro</a> and <a href="https://stackblitz.com" target="_blank" rel="noopener noreferrer">StackBlitz</a>.

Ideally it will be the only multilingual site in the near future.

### 2. Peter the Great and the Netherlands
Guide on the traces of Peter the Great in the Netherlands and his influence on the development of Russia.

🚧This project is under development🚧
- [x] project plan 
- [x] collect literature on Peter's life and work
- [x] structure information on each year of Peter’s life
- [ ] prepare the database for the website
- [ ] create and maintain a website

### 3. Development of a site for natural clean water 
The <a href="https://www.rain-water.ru" target="_blank" rel="noopener noreferrer">RAIN</a> brand was designed to give people not only a pleasant taste of water, but also the feeling of a real quality product in glass with a wooden lid.

This project has unfortunately been closed. But it was great to work for a start-up brand.

The website was built on <a href="https://www.tilda.cc" target="_blank" rel="noopener noreferrer">Tilda</a>

### 4. Coming soon...

# <a id="Links"></a>Links 🔗 #

<p>Mail me to <a href="mailto:kirill@sundyrtsev.com" target="_blank" rel="noopener noreferrer">kirill@sundyrtsev.com</a></p>

  Message me in <a href="https://keybase.io/kiriller" target="_blank" rel="noopener noreferrer">Keybase</a>


<p>Check out my <a href="https://vsco.com/krllpht" target="_blank" rel="noopener noreferrer">VSCO</a></p>


## ⏭️
Please <a href="/other" target="_blank" rel="noopener noreferrer">click here</a> if you wish to go to another page.



<a class="back-to-top w-inline-block" href="/info" target="_self">
<p style="text-align: center;"><sub><sup>
<img src="/images/up.svg" width="25" height="36" loading="lazy" alt=""> </sup></sub></p>
</a>


<p style="text-align: center;"><sub><sup>This site was built with the help of Hugo theme Pulp and Gitlab.</sup></sub></p>
