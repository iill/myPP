
# <a id="Hobby"></a>Hobby ❤️ #
Sometimes I want to see the beauty of this world from the height of a bird’s flight, in this helps me my friend named <a href="https://vimeo.com/kirilldji" target="_blank" rel="noopener noreferrer">Grayhawk</a>


# Travel 🗺️
Travel as an integrated part of life, which helps to reboot yourself and learn new things in the world. Each journey provides me with a new experience and invaluable knowledge.

Check interactive Map of the best places I visited on 
<a href="https://www.polarsteps.com/siberian" target="_blank" rel="noopener noreferrer">Polarsteps</a>

# Favorite services ⭐
## Every day use
A  powerful and simple bookmark manager available on multiple platforms. Organize your bookmarks and access them quickly - <a href="https://raindrop.io/" target="_blank" rel="noopener noreferrer">Raindrop.io</a>

A convenient tool for quick notes in a separate browser tab. Capture thoughts and ideas on the fly - <a href="https://www.manifest.app/" target="_blank" rel="noopener noreferrer">Manifest</a> 

Analyze suspicious files, domains, IP addresses, and URLs for malware and other threats. Protect your devices from dangers - <a href="https://www.virustotal.com" target="_blank" rel="noopener noreferrer">Virus Total</a> 

## Travel
The most convenient service for step-by-step recording of your trips with photos. Save your best travel moments and share them with friends - <a href="https://www.polarsteps.com" target="_blank" rel="noopener noreferrer">Polarstep</a>

## For the soul
Dive into the unique vibe of Poolsuite FM with its stylish web design. It offers a few carefully curated stations for relaxing background music - <a href="https://poolsuite.net/" target="_blank" rel="noopener noreferrer">Poolsuite FM </a>

Create drawings in a simple interface on your phone or computer - <a href="https://okso.app/" target="_blank" rel="noopener noreferrer">OkSo</a>


## ⬅️
If you want to return to the info page <a href="/info" target="_blank" rel="noopener noreferrer">click here</a>, please.

---
<a class="back-to-top w-inline-block" href="#Hobby">
<p style="text-align: center;"><sub><sup>
<img src="/images/up.svg" width="25" height="36" loading="lazy" alt=""> </sup></sub></p>
</a>

<p style="text-align: center;"><sub><sup>This site was built with the help of Hugo theme Pulp and Gitlab.</sup></sub></p>