## Home page 

![Main page](/images/mane_page.png 'Main Page')
## Family crest

Here you can notice that the site did not have time to download and therefore the screenshot did not work with all the contents. It’s not normal for our time, sites should be faster. My mistake was that I didn’t optimize the images and used third-party extensions, which affected the page loading speed. 

![gerb](/images/gerb.png 'gerb')
## History

![Main page](/images/history.png 'History')
## Map

![Main page](/images/map.png 'Map')
## Gallery

![Main page](/images/gallery.png 'Gallery')
